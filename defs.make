# Installation paths for use in debian/rules of Matlab-related packages
MATLAB_MDIR = /usr/share/matlab/site/m
MATLAB_MEX_BINDIR = /usr/lib/matlab/site
MATLAB_MEX_SRCDIR = /usr/src/matlab
